#include <GyverOS.h>
#include <AnalogKey.h>
#include <GyverButton.h>

//Вкл/выкл отладочных сообщений
//#define DEBUG_ENABLE

#ifdef DEBUG_ENABLE
#define DEBUG(x) Serial.println(x)
#else
#define DEBUG(x)
#endif

//Пин управления передними ПТФ
#define FR_FOG_PIN 2

//Пин управления задней ПТФ
#define RR_FOG_PIN 3

//Длительность "нажатия" кнопки ПТФ
#define FOG_BTN_IMPULSE 500

//Пин переднего датчика ОЖ
#define FR_SENS_PIN 4

//Пин переднего индикатора
#define FR_IND_PIN 5

//Пины заднего датчика ОЖ
#define RR_SENS_PIN 8

//Пин заднего индикатора
#define RR_IND_PIN 9

//Периоды опроса датчиков
#define FR_SENS_PERIOD 5000
#define RR_SENS_PERIOD 5000

//Количество одинаковых состояний датчика для изменения состояния индикатора
#define CNT_THRESHOLD 5

//Подключение "кольца" ПТФ
AnalogKey<A0, 2> keys;

GButton fogOff, fogOn;
GyverOS<2> OS;
int8_t frCounter = 0;
int8_t rrCounter = 0;

void setup() { 
  #ifdef DEBUG_ENABLE
    Serial.begin(9600);
  #endif

  //Сопротивление аналоговых кнопок 
  keys.attach(0, 45);
  keys.attach(1, 0);

  pinMode(FR_FOG_PIN, OUTPUT);
  pinMode(RR_FOG_PIN, OUTPUT);
  pinMode(FR_IND_PIN, OUTPUT);
  pinMode(RR_IND_PIN, OUTPUT);
  pinMode(FR_SENS_PIN, INPUT_PULLUP);
  pinMode(RR_SENS_PIN, INPUT_PULLUP);

//Моргаем индикаторами при запуске 3 раза
  for (int i = 0; i <= 5; i++) {
    digitalWrite(FR_IND_PIN, !digitalRead(FR_IND_PIN));
    digitalWrite(RR_IND_PIN, !digitalRead(RR_IND_PIN));
    delay(333);
  }

//Простой опрос датчиков при запуске, вкл. индикаторы сразу, если уровень ОЖ низкий
  if (digitalRead(FR_SENS_PIN) == 0) {
    digitalWrite(FR_IND_PIN, 1);
  }
  if (digitalRead(RR_SENS_PIN) == 0) {
    digitalWrite(RR_IND_PIN, 1);
  }

//Запускаем опрос датчиков по алгоритму
  OS.attach(0, frSensorHandler, FR_SENS_PERIOD);
  OS.attach(1, rrSensorHandler, RR_SENS_PERIOD);
  OS.start(0);
  OS.start(1);
}
  
void loop() {
// DEBUG(analogRead(A0));
// delay(200);
  static boolean isFrontOn = false;
  static boolean isRearOn = false;
  OS.tick();
  fogOn.tick(keys.status(1));
  fogOff.tick(keys.status(0));

  if (fogOn.isPress()) {
    if (!isFrontOn) {
      digitalWrite(FR_FOG_PIN, 1);
      delay(FOG_BTN_IMPULSE);
      digitalWrite(FR_FOG_PIN, 0);
      isFrontOn = true;
      DEBUG("Front Fog Lamps ON!");
    } else if (isFrontOn && !isRearOn) {
      digitalWrite(RR_FOG_PIN, 1);
      delay(FOG_BTN_IMPULSE);
      digitalWrite(RR_FOG_PIN, 0);
      isRearOn = true;
      DEBUG("Front & Rear Fog Lamps ON!");
    }
  } else if (fogOff.isPress()) {
    if (isRearOn) {
      digitalWrite(RR_FOG_PIN, 1);
      delay(FOG_BTN_IMPULSE);
      digitalWrite(RR_FOG_PIN, 0);
      isRearOn = false;
      DEBUG("Front Fog Lamps ON & Rear Fog Lamps OFF!");
    } else if (isFrontOn) {
      digitalWrite(FR_FOG_PIN, 1);
      delay(FOG_BTN_IMPULSE);
      digitalWrite(FR_FOG_PIN, 0);
      isFrontOn = false;
      DEBUG("Front Fog Lamps OFF & Rear Fog Lamps OFF!");
    }
  }

  indicate(frCounter, FR_IND_PIN);
  indicate(rrCounter, RR_IND_PIN);
}

//Обработчик переднего датчика ОЖ
void frSensorHandler() {
  pollSensor(FR_SENS_PIN, FR_IND_PIN, frCounter);
}

//Обработчик заднего датчика ОЖ
void rrSensorHandler() {
  pollSensor(RR_SENS_PIN, RR_IND_PIN, rrCounter);
}

//Опрашивает указанный датчик и изменяет значение счетчика, в зависимости от состояния индикатора 
void pollSensor(byte sensPin, byte indPin, int8_t& counter) {
  if (digitalRead(sensPin) == 0) {
    if (digitalRead(indPin) == 0) {      
      DEBUG("SW is ON");
      DEBUG(counter);
      counter++;
    } else {        
      DEBUG("Ind is On, no change");
      counter = CNT_THRESHOLD;
    }
  } else if (digitalRead(sensPin) == 1) {
    if (digitalRead(indPin) == 1) {
      DEBUG("SW is OFF");
      DEBUG(counter);
      counter--;
    } else {
      DEBUG("Ind is off, no change");
      counter = 0;
    }
  }
}

//Управляет индикатором на указанном пине в зависимости от значения счётчика
void indicate(int8_t counter, byte indPin) {
  if (counter > CNT_THRESHOLD) {
    digitalWrite(indPin, 1);
  } else if (counter < 0){
    digitalWrite(indPin, 0);
  }   
}